<?php
//打印good->source数据，人工观察匹配情况
include('lib/db.php');
$db = new db();
$db->Enq('use test;');

$sql = 'SELECT object_id,COUNT(0) AS num FROM object_mapping GROUP BY object_id HAVING num > 1 ORDER BY num desc';
$good_ids = $db->Ec($sql);

//循环取出所有good->source数据
$info_list = array();
foreach($good_ids as $good_id) {
    $sql = "SELECT * FROM source_product_for_mapping s INNER JOIN object_mapping m WHERE m.domain_id = s.domain_id AND m.source_id = s.source_id AND m.object_id = {$good_id}";
    $source_list = $db->Eq($sql);
    
    $sql = "select * from goods where id = {$good_id}";
    $good_info = $db->Eor($sql);
    $cat_id = $good_info['cat_id'];
        
    if(empty($info_list[$cat_id])) {
       $info_list[$cat_id] = array(); 
    }

    $info_list[$cat_id][$good_info['id']] = array('good'=>$good_info, 'source'=>$source_list);
}


//展示
$table = '';
foreach($info_list as $cat_id=>$cat_ful) {
    $table .= '<div class="cat" style="width:900px;margin:15px auto 0;padding:5px 0 0 150px;border-top:1px solid black;">小类ID: ' . $cat_id . '</div>';
    foreach($cat_ful as $good_id=>$good_ful) {
        $table .= make_table($good_ful);
    }
}


$html = <<<doc
<!DOCTYPE HTML>
<html>
<meta charset="utf-8">
<head>
<title>正式环境模拟</title>
</head>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.6.4.min.js"></script>
<body>
{$table}

<script type="text/javascript">

$(document).ready(function(){
    $('.rate').bind('keyup', function(){
        var p = $(this).parents('div');
        var t = p.children('table');
        if($(this).val() == '') {
            p.css('background-color', '#bbb');
            t.css('background-color', '#fff');
        } else if($(this).val() == '100') {
            p.css('background-color', '#ABD686');
            t.css('background-color', '#fff');
        } else {
            p.css('background-color', '#f66');
            t.css('background-color', '#fff');
        }
    });

});
</script>
</body>
</html>
doc;

echo $html;

function make_table($good_ful) {
    $good_info = $good_ful['good'];
    $source_info = $good_ful['source'];    
    
    $table = '';
    $table .= '<div class="good" style="width:800px;margin:0 auto 5px;padding:0 0 5px 50px;border-bottom:1px solid #999;">';
    $table .= '<table border="1">';
    $table .= '<tr>';
    $table .= '<td>&nbsp;' . $good_info['id'] . '</td>';
    $table .= '<td>&nbsp;' . $good_info['name'] . '</td>';
    $table .= '<td>&nbsp;' . $good_info['good_sn'] . '</td>';
    $table .= '<td>&nbsp;' . $good_info['goods_model'] . '</td>';
    $table .= '<td>&nbsp;匹配度: <input class="rate" type="text" style="width:40px;color:red;font-weight:bold;"></td>';
    $table .= '</tr>';
    $table .= '</table>';

    $table .= '<table border="1">';
    foreach($source_info as $row) {
        $table .= '<tr>';
        $table .= '<td>&nbsp;' . $row['product_name_full'] . '</td>';
        $table .= '<td>&nbsp;' . $row['product_name'] . '</td>';
        $table .= '</tr>';
    }
    $table .= '</table>';
    $table .= '</div>';
    
    return $table;
}






