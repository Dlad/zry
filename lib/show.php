<?php

function show_list($rows, $title) {
    $tbody = "";
    foreach($rows as $row) {
        $tbody .= '<tr>';
        foreach($row as $col) {
            $tbody .= "<td>{$col}&nbsp;</td>";
        }
        $tbody .= '</tr>';
    }
   
    $table = ''; 
    $table .= "<div style=\"width:800px;margin:15px auto;text-align:center;\">{$title}</div>";
    $table .= "<table border=\"1\" style=\"width=800px;margin:15px auto;\">{$tbody}</table>";
    
    return $table;
}

function show_input($name, $type, $title = '', $ph = '') {
    $html = '<div style="width=800px;margin:15px auto;">' . $title . ': <input style="width:400px;height:20px;" type="' . $type . '" name="' . $name . '" value="' . $ph . '" /></div>';
    return $html;
}
