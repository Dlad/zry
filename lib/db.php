<?php
define('__DATABASE_CONNECT_STRING','mysql:host=220.170.79.16;');
//封装一些常用函数
class db {
    static protected $pdo;
    
    function __construct() {
        $this->GetInstance();
    }
    
    //获取唯一的数据库连接对象
    function GetInstance() {
        if (self::$pdo == null) {
        	self::$pdo = new PDO(__DATABASE_CONNECT_STRING, 'zuirongyiremote', 'zuirongyimylove');
        	self::$pdo->exec('SET NAMES UTF8');
//        	var_dump("once");
        }
        
        return self::$pdo;
    }
    
    //执行sql语句，返回结果矩阵
    function Eq($sql) {
        $res = self::$pdo->query($sql);
        //如果存在，打印错误信息
        $this->IsError();
        $res->setFetchMode(PDO::FETCH_ASSOC);
        return $res->fetchAll();
    }
    
    //执行sql语句，返回单行结果
    function Eor ($sql) {
        $res = self::$pdo->query($sql);
        //如果存在，打印错误信息
        $this->IsError();
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $data = $res->fetch();
        return $data;
    }
    
    //执行sql语句，返回单列结果
    function Ec($sql) {
        $res = self::$pdo->query($sql);
        //如果存在，打印错误信息
        $this->IsError();
        $res->setFetchMode(PDO::FETCH_ASSOC);
        $data = $res->fetchAll(PDO::FETCH_COLUMN, 0);
        return $data;
    }
    
    //执行sql语句，返回数量(单个结果)
    function Es($sql) {
        $res = self::$pdo->query($sql);
        //如果存在，打印错误信息
        $this->IsError();
        $res->setFetchMode(PDO::FETCH_NUM);
        $data = $res->fetch(PDO::FETCH_COLUMN);
        return $data;
    }
    
    //执行sql语句，返回影响行数
    function Enq($sql) {
        $res = self::$pdo->exec($sql);
        //如果存在，打印错误信息
        $this->IsError();
        
        return $res;
    }
    
    //todo：貌似用不到啊，直接用pdo对象吧
    //执行sql语句，返回影响行数
    function ExecuteNoneQueryWithPrepare($sql) {
        
    }
    
    //调试信息，发布后可关闭
    function IsError() {
        $errorCode = self::$pdo->errorCode();
        if($errorCode != '00000') {
            var_dump(self::$pdo->errorInfo());
            exit();
        }
    }
    
    //添加引号
    function quote($var) {
        return self::$pdo->quote($var);
    }
}

?>
