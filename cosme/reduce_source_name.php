<?php
/*
* 据product_name_full和brand_name产生product_name
* 便于sphinx索引
*/

set_time_limit(0);
include_once '../lib/db.php';
$db = new db();
$db->Enq('use test;');

$limit = 0;
$step = 1000;
do{
    $sql = "SELECT s.id, s.product_name_full, b.name, b.name_en FROM source_product_for_mapping s INNER JOIN brand b WHERE s.product_brand_id = b.id AND s.product_category_id IN (SELECT id FROM category WHERE path LIKE ',522,%') AND s.domain_id <> 26 order by s.id asc limit {$limit},{$step}";
    $rows = $db->Eq($sql);

    if(!empty($rows)) {
        foreach($rows as $row) {
            //$r = explode("\n", trim($row['name']));
            //$sn = array_pop($r);

            //去除产品名称中的品牌名称
            $pname = trim($row['product_name_full']);
            $bname = $row['name'];
            $bname_en = $row['name_en'];
            if(mb_strlen($bname, 'utf-8') > 2) {
                $pname = preg_replace("~{$bname}~is", '', $pname);
            }
            if(mb_strlen($bname_en, 'utf-8') > 2) {
                $pname = preg_replace("~{$bname_en}~is", '', $pname);
            }
           
            //去除容量信息
            $pname = preg_replace('%([0-9.]+)(ml|g|粒|色)%is',  '', $pname);
            //去除空格
            $pname = preg_replace('%\s+%', '', $pname);

            $pname = addslashes($pname);
            $sql = "update source_product_for_mapping set product_name='{$pname}' where id = {$row['id']}";

            $sql . "\n";
            $db->Enq($sql);
        }
    }
    $limit += $step;
} while(!empty($rows));


