<?php
/*
* 据full_name和brand_name产生good_sn
* 便于sphinx索引
*/

set_time_limit(0);
include_once '../lib/db.php';
$db = new db();
$db->Enq('use test;');

$limit = 0;
$step = 1000;
do{
    $sql = "SELECT g.id,g.name,b.name AS bname, b.name_en FROM goods g INNER JOIN brand b WHERE b.id = g.brand_id and g.cat_id in (select id from category where path like '%,522,%') order by id asc limit {$limit},{$step}";
    $rows = $db->Eq($sql);

    if(!empty($rows)) {
        foreach($rows as $row) {
            //$r = explode("\n", trim($row['name']));
            //$sn = array_pop($r);

            //去除产品名称中的品牌名称
            $sn = trim($row['name']);
            $bname = $row['bname'];
            $bname_en = $row['name_en'];
            if(mb_strlen($bname, 'utf-8') > 2) {
                $sn = preg_replace("~{$bname}~is", '', $sn);
            }
            if(mb_strlen($bname_en, 'utf-8') > 2) {
                $sn = preg_replace("~{$bname_en}~is", '', $sn);
            }
           
            //去除容量信息
            $sn = preg_replace('%([0-9.]+)(ml|g|粒|色)%is',  '', $sn);
            //去除空格
            $sn = preg_replace('%\s+%', '', $sn);

            $sn = addslashes($sn);
            $sql = "update goods set good_sn='{$sn}' where id = {$row['id']}";

            $db->Enq($sql);
        }
    }
    $limit += $step;
} while(!empty($rows));


