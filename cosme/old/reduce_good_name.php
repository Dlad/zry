<?php
/*
据full_name和brand_name产生good_sn
便于sphinx索引
*/

set_time_limit(0);
include_once 'lib/db.php';
$db = new db();

$limit = 0;
$step = 1000;
do{
    $sql = "select id,name from test.goods where cat_id in (select id from compare_pric.category where path like '%,522,%') order by id asc limit {$limit}, {$step}";
    $rows = $db->Eq($sql);

    if(!empty($rows)) {
        foreach($rows as $row) {
            $r = explode("\n", trim($row['name']));
            $sn = array_pop($r);
            
            
            $sn = addslashes($sn);
            $sql = "update test.goods set good_sn='{$sn}' where id = {$row['id']}";

            $db->Enq($sql);
        }
    }
    $limit += $step;
} while(!empty($rows));



