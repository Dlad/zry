<?php
//利用sphinx生成map_good2source
//cosme

set_time_limit(0);

include_once 'lib/db.php';

//数据库连接
$db = new db();
$sql = 'SELECT *,COUNT(source_id) AS num FROM cosme.pre_map_1 GROUP BY source_id HAVING num = 1';
$rows = $db->Eq($sql);

//sphinx
$sphinx = new SphinxClient();
$sphinx->SetServer('localhost', 9312);   
//$sphinx->SetMatchMode(SPH_MATCH_ALL);
$sphinx->SetMatchMode(SPH_MATCH_ANY);

foreach($rows as $row) {
    $query = $row['product_name'];
    $r1 = $sphinx->query($query, 'mysql');
    $ids1 = get_id($r1);

    $map = array();
    if(!empty($ids1)) {
        foreach($ids1 as $id) {
            $weight = $r1['matches'][$id]['weight'];
            $map[$id] = $weight;
	    arsort($map);
        }

        $ids = filter_by_weight($map);
	
	if(!empty($ids)) {
	    mapping($row['source_id'], $ids);
	}
    }
    
}



function get_id($r) {
    return empty($r['matches']) ? array() : array_keys($r['matches']);
}

//根据权重返回id
function filter_by_weight($id_to_weight) {
    $weight_max = max($id_to_weight);
    if($weight_max < 20) {
        return array();
    }

    $ids = array();
    foreach($id_to_weight as $id=>$weight) {
        $rate = $weight/$weight_max;
	if($rate >= 0.75) {
	   $ids[] = $id; 
	}
    }
    return $ids;
}

//object_mapping
function mapping($id, $ids) {
    $db = new db();

    $sql = "INSERT INTO cosme.pre_map_2 VALUES ('', '{$id}', '" . implode('|', $ids) . "')";
    $db->Enq($sql);
}







