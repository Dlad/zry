<?php
set_time_limit(0);

//sphinx
$map = array('any'=>SPH_MATCH_ANY, 'all'=>SPH_MATCH_ALL, 'bool'=>SPH_MATCH_BOOLEAN, 'extended2'=>SPH_MATCH_EXTENDED2);
$sphinx = new SphinxClient();
$sphinx->SetServer('localhost', 9312);
$sphinx->SetMatchMode($map['any']);

include_once 'lib/db.php';
$db = new db();
$noise = array('特别版'=>'', '限量版'=>'', '优惠套装'=>'');
$max = 4025;
$limit = 0;
$step = 1000;

for($limit=0; $limit < $max ; $limit+=$step) {
    $sql = "select id,product_name,product_name_full,product_brand_id as brand_id,source_url,domain_id,source_id from test.source_product_for_mapping order by id asc limit {$limit}, {$step}";
    $rows = $db->Eq($sql);

    if(!empty($rows)) {
        foreach($rows as $row) {
            $key_word = (empty($row['product_name']) || mb_strlen($row['product_name'], 'utf-8')<5) ? $row['product_name_full'] : $row['product_name'];
	    
  	    $ids = array();
            if(!empty($row['brand_id'])) {
                $sphinx->setFilter('brand_id', array($row['brand_id']));
	
		//todo:干脆只做good_sn全文索引	
		//$r = $sphinx->query('@good_sn' . $key_word, 'good');
		$r = $sphinx->query($key_word, 'good');

		if(!empty($r['matches'])) {
		    $ids = first_group($r['matches']);

		    if(!empty($ids)) {
		        //是否出现'套装'字样，good source需保持一致
		        $sql = 'select id, good_sn from test.goods where id in (' . implode(',', $ids) . ')';
		        $good_sn_list = $db->Eq($sql);

		        $suit = array('件套'=>'', '套装'=>'', '合一'=>'', '礼盒'=>'', '套刷'=>'', '套盒'=>'', '组合'=>'');
			$is_suit = array('y'=>array(), 'n'=>array());
			foreach($good_sn_list as $sn_info) {
			    if(strlen($sn_info['good_sn']) != strlen(strtr($sn_info['good_sn'], $suit))) {
				array_push($is_suit['y'], $sn_info['id']);
			    } else {
				array_push($is_suit['n'], $sn_info['id']);
			    }
			}

		        if(strlen($key_word) != strlen(strtr($key_word, $suit))) {
		           $ids = $is_suit['y']; 
		        } else {
		           $ids = $is_suit['n']; 
		        }

		        //只要容量相等的good_id
		        $model_match = true;
                        if(preg_match('%([0-9.]+)(ml|g)%i', $row['product_name_full'], $match)) {
                            $source_model = $match[1];
                            if(!empty($ids)) {
                                $sql = 'SELECT id,goods_model FROM test.goods WHERE id in (' . implode(',', $ids) . ') and goods_model <> ""';
		                $good_models = $db->Eq($sql);
		                $good_models = empty($good_models) ? array() : $good_models;
		                foreach($good_models as $model) {
		            	$model_match = false;
		            	preg_match('%([0-9.]+)(ml|g)%i', $model['goods_model'], $match);
		            	$good_model = empty($match) ? '' : $match[1];
		            	if(!empty($good_model) && $good_model == $source_model) {
		            	    $ids = array($model['id']);
		            	    $model_match = true;
		            	    break;
		            	}
		                }
		            }
                        }

		        if($model_match && !empty($ids)) {
		            $ids = array($ids[0]);
		        } else {
		            $ids = array();
		        }
		    }
		} 
            }
	    
	    if(!empty($ids)) {
		$now = date('Y-m-d H:i:s');
	        $row_ar = array();
                foreach($ids as $id) {
	            array_push($row_ar, "('', {$id}, '1', '{$row['domain_id']}', '{$row['source_id']}', '{$row['source_url']}', '{$now}', '0', '0')");
                }	    

	        $sql = 'insert into test.object_mapping values' . implode(',', $row_ar);
	        $db->Enq($sql);
	    }

            $sphinx->resetFilters();
        }
    }
}

function first_group($rows) {
    $ids = array();    

    $id2weight = array();
    foreach($rows as $id=>$row) {
	$id2weight[$id] = $row['weight'];
    }
    arsort($id2weight);
    $max = key($id2weight);
    if($id2weight[$max] > 9 && !empty($max)) {
	array_push($ids, $max);

        foreach($id2weight as $id=>$weight) {
            if($id2weight[$max] == $weight) {
	        array_push($ids, $id);
	    }
        }
    }

    return $ids;
}

function get_id($r) {
    return empty($r['matches']) ? array() : array_keys($r['matches']);
}
