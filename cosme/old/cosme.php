<?php
set_time_limit(0);
include 'lib/db.php';

//db
$db = new db();
$sql = 'select id, name_zh, name_en from test.brand';
$rows = $db->Eq($sql);

//sphinx
$sphinx = new SphinxClient();
$sphinx->SetServer('localhost', 9312);

$count = count($rows);
for($i=0; $i<$count; $i++) {
    $brand_id = $rows[$i]['id'];
    $name_en = $rows[$i]['name_en'];
    $name_zh = $rows[$i]['name_zh'];

    $sphinx->SetMatchMode(SPH_MATCH_ALL);
    
    $r1 = empty($name_en) ? array() : $sphinx->query($name_en, 'mysql');
    $r2 = empty($name_zh) ? array() : $sphinx->query($name_zh, 'mysql');

    $ids1 = get_id($r1);
    $ids2 = get_id($r2);

    $ids = array_merge($ids1, $ids2);
    $ids = array_unique($ids);
    
    if(!empty($ids)) {
        $sql = 'SELECT id,product_name_full FROM test.source_product_for_mapping WHERE id IN (' . implode(',', $ids) . ')';
        $source_info_list = $db->Eq($sql);
	foreach($source_info_list as $source_info) {
	    $product_name = $source_info['product_name_full'];
	    $product_name = preg_replace("/$name_en/i", '', $product_name, -1);
	    $product_name = preg_replace("/$name_zh/i", '', $product_name, -1);
	    $product_name = trim(addslashes($product_name));
	    $sql = "INSERT INTO cosme.pre_map_1 VALUES('', '{$source_info['id']}', '{$brand_id}', '{$product_name}')"; 
            $db->Enq($sql);
    	}

    }
}


function get_id($r) {
    return empty($r['matches']) ? array() : array_keys($r['matches']);
}

