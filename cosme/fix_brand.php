<?php
define ( "COOKIE_FILE", dirname(__FILE__) . '/cookie.txt' );

//重新抓取数据，修复brand表
include_once '../lib/db.php';
$db = new db();
$db->Enq('use test;');

$limit = 0;
$step = 1000;

do {
    $sql = "SELECT id,site_url FROM brand WHERE site_url LIKE 'http://cosme.pclady.com.cn%' order by id asc limit {$limit},{$step}";
    $url_list = $db->Eq($sql);
   
    foreach($url_list as $row) {
        $id = $row['id'];
        $url = $row['site_url'];
        $bname = '';        
        $bname_en = '';        

        $c = get_url_content($url);
        $r = preg_match('%中文名称：<span class="sGray">(?P<bname>[^<]+)%', $c, $match);
        if($r) {
            $bname = $match['bname'];
        }
        $r = preg_match('%英文名称：<span class="sGray">(?P<bname_en>[^<]+)%', $c, $match);
        if($r) {
            $bname_en = $match['bname_en'];
        }

        if(!empty($bname) || !empty($bname_en)) {
            $bname = addslashes($bname);
            $bname_en = addslashes($bname_en);
            $sql = "update brand set name='{$bname}', name_en='{$bname_en}' where id = {$id} ";
            
            $db->Enq($sql);
        }

    }

    $limit += $step;
} while(!empty($url_list));


function req_url($url = "", $post_data = NULL) {
    $cookie_jar = COOKIE_FILE;
    $res = curl_init ();
    curl_setopt ( $res, CURLOPT_URL, $url );
    curl_setopt ( $res, CURLOPT_HEADER, 0 );
    curl_setopt ( $res, CURLOPT_RETURNTRANSFER, 1 );
    curl_setopt ( $res, CURLOPT_TIMEOUT, 20);
    curl_setopt ( $res, CURLOPT_ENCODING, "identity" );
    curl_setopt ( $res, CURLOPT_COOKIEFILE, $cookie_jar );
    curl_setopt ( $res, CURLOPT_COOKIEJAR, $cookie_jar );
    
    //User-Agent    Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13
    //curl_setopt ( $res, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 2.0.50727;)' );
    curl_setopt ( $res, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; zh-CN; rv:1.9.2.13) Gecko/20101203 Firefox/3.6.13;)' );
    if ($post_data != NULL && ! empty ( $post_data )) {
        //post
        curl_setopt ( $res, CURLOPT_POST, 1 );
        curl_setopt ( $res, CURLOPT_POSTFIELDS, $post_data );
    }
    $tt = curl_exec ( $res );
    $code = curl_getinfo ( $res, CURLINFO_HTTP_CODE );
    //缓存http状态
    curl_close ( $res );
    if ($code >= 200 && $code < 400) {
        return $tt;
    } else {
        @file_put_contents ( "log_error_url", "\r\n\r\n***************************\r\n\r\n$code.[" . date ( 'l' ) . "]=>$url   \r\n$tt", FILE_APPEND );
        return false;
    }
}

function get_url_content($url, $s_charact = "gbk", $d_charact = "UTF-8//IGNORE") {
    //echo $s_charact;
    $urlContent = req_url ( $url );
    
    if (! $urlContent) {
        return FALSE;
    }

    if ($s_charact == $d_charact) {
        return $urlContent;
    }
    $content = iconv ( $s_charact, $d_charact, $urlContent );
    return $content;
}
