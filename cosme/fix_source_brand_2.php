<?php
/*
*  修复source表品牌id
*  遍历品牌，若在source品牌名称中出现，为其修复品牌id
*/
include_once '../lib/db.php';

$db = new db();
$db->Enq('use test;');

$sql = "SELECT id,name,name_en FROM brand WHERE site_url LIKE 'http://cosme.pclady.com.cn%' ORDER BY LENGTH(NAME) asc";
$brand_list = $db->Eq($sql);

$sql = "SELECT distinct product_brand_name FROM source_product_for_mapping WHERE product_brand_id = 0 OR product_brand_id IS NULL";
$unbrand_list = $db->Ec($sql);

foreach($unbrand_list as $unbrand) {
    //定义品牌同义词
    $brand_syno_map = array('BOSS'=>'22980', 'Boss 波士'=>'22980', 'DODO'=>'22730', 'LOREAL 欧莱雅'=>'22489', 'Skin 79'=>'22964', 'The face shop彩妆'=>'22696', 'TONY MOLY'=>22867, 'Y.S.L 伊夫.圣罗兰'=>22667, 'ZA'=>22536, '玉兰油'=>22529, '古驰'=>22790, '欧莱雅彩妆'=>22489, '欧莱雅专业洗发'=>23625, '韩国SKIN FOOD'=>22603, 'Biosecure安悦'=>898, 'PHILIPS 飞利浦'=>13, 'S.T.Dupont 都彭'=>5302);
    
    if(array_key_exists($unbrand, $brand_syno_map)) {
        $bid = $brand_syno_map[$unbrand];
        $sql = "UPDATE source_product_for_mapping SET product_brand_id = {$bid} WHERE product_brand_name = '{$unbrand}' and (product_brand_id = 0 or product_brand_id is null)";
        $db->Enq($sql);
        
        continue;
    } 

    foreach($brand_list as $brand) {
        $bid = $brand['id'];
        $bname = $brand['name'];
        $bname_en = $brand['name_en'];
       
        if(false !== strpos($unbrand, $bname) || (!empty($bname_en) && false !== strpos($unbrand, $bname_en))) {
	        $unbrand = addslashes($unbrand);
            $sql = "UPDATE source_product_for_mapping SET product_brand_id = {$bid} WHERE product_brand_name = '{$unbrand}' and (product_brand_id = 0 or product_brand_id is null)";
	        $db->Enq($sql);
	        break;
        } else {
            continue;
        }
    }
}
