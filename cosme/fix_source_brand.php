<?php
/*
* 修复sp4m表品牌id
* 使用source品牌名称在品牌表中搜索对应id(like模式)
*/
set_time_limit(0);
include_once '../lib/db.php';
$db = new db();
$db->Enq('use test;');

$limit = 0;
$step = 1000;

do {
    $sql = "select id, product_brand_name from source_product_for_mapping where (product_brand_id = 0 or product_brand_id is null) and product_brand_name <> '' and product_brand_name is not null and product_category_id IN (SELECT id FROM category WHERE path LIKE '%,522,%') order by id asc limit {$limit},{$step}";
    
    $rows = $db->Eq($sql);
    foreach($rows as $row) {
        $bname = addslashes($row['product_brand_name']);
        $sql = "select id,name from brand where (name like '%{$bname}%' or name_en like '%{$bname}%') and site_url LIKE 'http://cosme.pclady.com.cn%' order by length(name) asc";
        $binfo = $db->Eor($sql);
        
        if(isset($binfo['name']) && !empty($binfo['name'])) {
            $sql = "update source_product_for_mapping set product_brand_id = {$binfo['id']} where id = {$row['id']}";
            $db->Enq($sql);
        }   
    }
    
    $limit += $step;
} while(!empty($rows));
