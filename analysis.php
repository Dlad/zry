<?php
//一个简单页面，返回不同模式sphinx的查询结果
error_reporting(E_ALL);
ini_set('display_errors', 1);

include_once 'lib/db.php';
include_once 'lib/show.php';

$db = new db();
$db->Enq('use compare_pric_1');

if(!empty($_POST)) {
    $strict_str = isset($_POST['all']) ? $_POST['all'] : '';
    $loose_str = isset($_POST['any']) ? $_POST['any'] : '';
    $sphinx_config_name = isset($_POST['sphinx_config_name']) ? $_POST['sphinx_config_name'] : 'product';

 
    $mode = array(SPH_MATCH_BOOLEAN, SPH_MATCH_EXTENDED2, SPH_MATCH_ANY, SPH_MATCH_ALL);
    $map = array('any'=>SPH_MATCH_ANY, 'all'=>SPH_MATCH_ALL, 'bool'=>SPH_MATCH_BOOLEAN, 'extended2'=>SPH_MATCH_EXTENDED2);

    $sphinx = new SphinxClient();
    $sphinx->SetServer('220.170.79.32', 9312);
    
    //宽松匹配
    if(!empty($loose_str)) {
        $sphinx->SetMatchMode($map['any']);
        $r1 = $sphinx->query($loose_str, $sphinx_config_name);

        $ids1 = get_id($r1);

        if(!empty($ids1)) {
	        if('product' == $sphinx_config_name) {
		        $rows = get_product_by_id($ids1);            
	        } else {
		        $rows = get_good_by_id($ids1);
	        }

	        append_weight(&$rows, $r1);
            $html_loose = show_list($rows, '宽松匹配:' . $loose_str);
        }
    } else {
        $ids1 = array();
        $html_loose = '';
    }
    
    //严格匹配
    if(!empty($strict_str)) {
        $sphinx->SetMatchMode($map['all']);
        $r2 = $sphinx->query($strict_str, $sphinx_config_name);
        $ids2 = get_id($r2);
        if(!empty($ids2)) {
            if('product' == $sphinx_config_name) {
            	$rows = get_product_by_id($ids2);
	    } else {
	        $rows = get_good_by_id($ids2);
            }

	    append_weight(&$rows, $r2);
            $html_strict = show_list($rows, '严格匹配:' . $strict_str);
        }
    } else {
        $ids2 = array();
        $html_strict = '';
    }


    //调试
    $sphinx->SetMatchMode($map['any']);
    //$sphinx->setRankingMode(SPH_RANK_BM25);
    $query_3 = isset($_POST['extended2']) ? $_POST['extended2'] : '';
    
    //todo: dump
    //$sphinx->setFilter('brand_id', array(22521));
    
    $r3 = $sphinx->query($query_3, $sphinx_config_name);
    //var_dump($r3);   
    $ids3 = get_id($r3);
    if(!empty($ids3)) {
        if('product' == $sphinx_config_name) {
            $rows = get_product_by_id($ids3);
        } else {
	        $rows = get_good_by_id($ids3);
        }
        append_weight(&$rows, $r3);
        $html_3 = show_list($rows, '调试匹配:' . $query_3);
    }
}

//展示
$html_loose = empty($html_loose) ? '' : $html_loose;
$html_strict = empty($html_strict) ? '' : $html_strict;
$html_3 = empty($html_3) ? '' : $html_3;
$r_str = $html_loose . $html_strict . $html_3;

$inputs = '';
$inputs .= show_input('any', 'text', '宽松匹配', $loose_str);
$inputs .= show_input('all', 'text', '严格匹配', $strict_str);
$inputs .= show_input('extended2', 'text', 'extend匹配', $query_3);

$check_pro = '';
$check_good = '';
if('product' == $sphinx_config_name) {
    $check_pro = 'checked';
} else {
    $check_good = 'checked';
}

$html = <<<doc
<!DOCTYPE HTML>
<html>
<meta charset="utf-8">
<head>
<title>sphinx搜索</title>
</head>
<script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.6.4.min.js"></script>
<body>
    <form method="POST" style="width:800px;margin:50px auto 15px;">
    <div style="width:400px;height:20px;">
        <span style="margin-right:25px;">表名:</span> 
        <input id="product" type="radio" name="sphinx_config_name" value="product" {$check_pro}><label for="product">product</label>
        <input id="good" type="radio" name="sphinx_config_name" value="good" {$check_good} ><label for="good">good</label>
    </div>
    {$inputs}
    <input type="submit" value="提交" style="margin:5px auto 15px;height:30px;" />
    </form>
    {$r_str}
</body></html>
doc;

echo $html;

function get_product_by_id($ids) {
    global $db;
    $sql = "select id,domain_id,product_name_full,product_category_name,product_brand_name,price from source_product_for_mapping where id in (" . implode(',', $ids) . ")";
    //$sql = "select id,domain_id,product_name_full,product_category_name,product_brand_name,price from source_product_for_mapping where id in (" . implode(',', $ids) . ") order by field(id, " . implode(',', $ids) . ")";
    $rows = $db->Eq($sql);

    return $rows;
}

function get_good_by_id($ids) {
    global $db;
    $sql = "select id,name,good_sn,market_price,goods_model from goods where id in (" . implode(',', $ids) . ") order by brand_id";
    
    $rows = $db->Eq($sql);
    return $rows;
}

function append_weight(&$rows, $r) {
    $count = count($rows);
    for($i=0; $i<$count; $i++) {
        $rid = $rows[$i]['id'];
        $weight = $r['matches'][$rid]['weight'];
        array_unshift($rows[$i], $weight);
    }

    //按照相关度排序结果
    $tem_ar = array();
    foreach($rows as $row) {
        $tem_ar[$row[0] . 'd' . rand(0, 100000000)] = $row;
    }
    arsort($tem_ar);
    $rows = $tem_ar;
}

function get_id($r) {
    return empty($r['matches']) ? array() : array_keys($r['matches']);
}
