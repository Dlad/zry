<?php
/*
* 部署改动正式环境中有
* todo:直接写过来
*/
set_time_limit(0);

//sphinx
$map = array('any'=>SPH_MATCH_ANY, 'all'=>SPH_MATCH_ALL, 'bool'=>SPH_MATCH_BOOLEAN, 'extended2'=>SPH_MATCH_EXTENDED2);
$sphinx = new SphinxClient();
$sphinx->SetServer('localhost', 9312);
$sphinx->SetMatchMode($map['any']);

include_once 'lib/db.php';
$db = new db();
$db->Enq('use test;');

$limit = 0;
$step = 1000;

do {
    $sql = "SELECT
              s.id,
              s.product_name,
              s.product_name_full,
              s.product_brand_id    AS brand_id,
              s.product_category_id AS cat_id,
              s.source_url,
              s.domain_id,
              s.source_id,
              b.name,
              b.name_en
            FROM source_product_for_mapping s
              INNER JOIN brand b
            WHERE s.product_brand_id <> 0
                AND s.product_brand_id IS NOT NULL
                AND s.product_brand_id = b.id
            ORDER BY id asc 
            limit {$limit}, {$step}";
    $rows = $db->Eq($sql);

    if(!empty($rows)) {
        foreach($rows as $row) {
            $key_word = $row['product_name_full'];
	        $noise = array('换购'=>'');
	        //超过2个汉字的品牌名称，须剔除避免冲淡权重
	        if(mb_strlen($row['name'], 'utf-8') > 1) {
	            $key_word = preg_replace("%{$row['name']}%i", '', $key_word, -1);
	        }
	        if(mb_strlen($row['name_en'], 'utf-8') > 1) {
	            $key_word = preg_replace("%{$row['name_en']}%i", '', $key_word, -1);
            }
	        $key_word = strtr($key_word, $noise);

            
	        echo "{$row['product_name_full']} \n";
	        echo "{$key_word} \n";
	        
  	        $ids = array();
	        $sphinx->setFilter('brand_id', array($row['brand_id']));
	        //$sphinx->setFilter('cat_id', array($row['cat_id']));
	        
	        $r = $sphinx->query($key_word, 'good');
	        if(!empty($r['matches'])) {
		        //获取相关度最高的一组值，并且该值高于一个定值
		        $ids = first_group($r['matches']);
		        
                if(!empty($ids)) {
		            //good与source的texture需保持一致
		            $sql = 'select id, good_sn from goods where id in (' . implode(',', $ids) . ')';
		            $good_sn_list = $db->Eq($sql);
                    
                    //定义同义词
                    $syno_map = array('女香'=>'女士香水', '男香'=>'男士香水', '香氛'=>'香水');
     
                    $sql = 'select name from texture';
                    $texture_map = $db->Ec($sql);
                    
                    $hit = array();
                    foreach($texture_map as $texture) {
                        $replaced = strtr($key_word, $syno_map);
                        if(strpos($replaced, $texture) !==  false) {
                            array_push($hit, $texture);
                        }
                    }
                    
                    foreach($good_sn_list as $sn_info) {
                        foreach($hit as $texture) {
                            $replaced = strtr($sn_info['good_sn'], $syno_map);
                            if(strpos($replaced, $texture) === false) {
                                $ids = array_diff($ids, array($sn_info['id']));
                                break;
                            }
                        }
                    }


                    //good与source套装属性一致
                    if(!empty($ids)) {
		                $sql = 'select id, good_sn from goods where id in (' . implode(',', $ids) . ')';
                        $good_sn_list = $db->Eq($sql);

                        $suit = array('件套'=>'', '套装'=>'', '合一'=>'', '礼盒'=>'', '套刷'=>'', '套盒'=>'', '组合'=>'');
		                $is_suit = array('y'=>array(), 'n'=>array());
		                foreach($good_sn_list as $sn_info) {
		        	        if(strlen($sn_info['good_sn']) != strlen(strtr($sn_info['good_sn'], $suit))) {
		        	            array_push($is_suit['y'], $sn_info['id']);
		        	        } else {
		        	            array_push($is_suit['n'], $sn_info['id']);
		        	        }
		                }

		                if(strlen($key_word) != strlen(strtr($key_word, $suit))) {
		                   $ids = $is_suit['y']; 
		                } else {
		                   $ids = $is_suit['n']; 
		                }
                    }

                    //todo
                    /*
                    //品牌与texture之间的部分,要完全一致
                    if(!empty($ids)) {
                        $sql = 'select id, good_sn from goods where id in (' . implode(',', $ids) . ')';
                        $good_sn_list = $db->Eq($sql);

                        $suit = array('件套'=>'', '套装'=>'', '合一'=>'', '礼盒'=>'', '套刷'=>'', '套盒'=>'', '组合'=>'');
                        $is_suit = array('y'=>array(), 'n'=>array());
                        foreach($good_sn_list as $sn_info) {
                            if(strlen($sn_info['good_sn']) != strlen(strtr($sn_info['good_sn'], $suit))) {
                                array_push($is_suit['y'], $sn_info['id']);
                            } else {
                                array_push($is_suit['n'], $sn_info['id']);
                            }
                        }

                        if(strlen($key_word) != strlen(strtr($key_word, $suit))) {
                           $ids = $is_suit['y'];
                        } else {
                           $ids = $is_suit['n'];
                        }
                    }
                    */


		            //只要容量相等的good_id
		            $model_match = true;
		            if(preg_match('%([0-9.]+)(ml|g|粒|色)%i', $row['product_name_full'], $match)) {
		        	    $source_model = $match[1];
		        	    if(!empty($ids)) {
		        	        $sql = 'SELECT id,goods_model FROM goods WHERE id in (' . implode(',', $ids) . ') and goods_model <> ""';
		        	        $good_models = $db->Eq($sql);
		        	        $good_models = empty($good_models) ? array() : $good_models;
		        	        foreach($good_models as $model) {
		        	    	    $model_match = false;
		        	    	    preg_match('%([0-9.]+)(ml|g|粒|色)%i', $model['goods_model'], $match);
		        	    	    $good_model = empty($match) ? '' : $match[1];
		        	    	    if(!empty($good_model) && abs($good_model - $source_model)<0.5) {
		        	    	        $ids = array($model['id']);
		        	    	        $model_match = true;
		        	    	        break;
		        	    	    }
		        	        }
		        	    }
		            }
                    
		            if($model_match && !empty($ids)) {
                        $id = array_shift($ids);
		        	    $ids = array($id);
		            } else {
		        	    $ids = array();
		            }
		        }
	        } 
	        
	        if(!empty($ids)) {
		        $now = date('Y-m-d H:i:s');
	            $row_ar = array();
                foreach($ids as $id) {
	                array_push($row_ar, "('', {$id}, '1', '{$row['domain_id']}', '{$row['source_id']}', '{$row['source_url']}', '{$now}', '0', '0')");
                }
		        $sql = 'insert into object_mapping values' . implode(',', $row_ar);
	            $db->Enq($sql);
	        }

            $sphinx->resetFilters();
        }
    }

    $limit += $step;
} while(!empty($rows));

function first_group($rows) {
    $ids = array();    

    $id2weight = array();
    foreach($rows as $id=>$row) {
	    $id2weight[$id] = $row['weight'];
    }
    arsort($id2weight);
    $max = key($id2weight);
    if($id2weight[$max] > 10 && !empty($max)) {
	    array_push($ids, $max);

        foreach($id2weight as $id=>$weight) {
            if($id2weight[$max] == $weight) {
	            array_push($ids, $id);
	        }
        }
    }

    return $ids;
}

function get_id($r) {
    return empty($r['matches']) ? array() : array_keys($r['matches']);
}
