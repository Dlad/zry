<?php
//利用sphinx生成map_good2source
//本次分类id414，镜头。将来视情况重构

set_time_limit(0);

include_once 'db.php';

//数据库连接
$m = new db();
$sql = 'select id from test.goods where cat_id = 414';
$gids = $m->Ec($sql);

//sphinx
$mode = array(SPH_MATCH_BOOLEAN, SPH_MATCH_EXTENDED2, SPH_MATCH_ANY, SPH_MATCH_ALL);
$sphinx = new SphinxClient();
$sphinx->SetServer('localhost', 9312);
$sphinx->SetFilter('domain_id', 4, true);

/*
$sphinx->SetMatchMode($mode[2]);
$r2 = $sphinx->query('尼康AF-S 300mm f/2.8D IF-ED II', 'mysql');

var_dump($r2);
exit;
*/

$count = count($gids);
for($i=0; $i<$count; $i++) {
    $sql = 'SELECT id,name,goods_model,url_aka,price,price_min,market_price,brand_id FROM test.goods WHERE id = ' . $gids[$i];
    $row = $m->Eor($sql);

    $sphinx->SetFilter('product_brand_id', array($row['brand_id']));
    preg_match('%([\d\.]+/[\d\.]+)%i', $row['name'], $match);
    if(!empty($match)) {
        $search_string = str_replace('/', ' ', $match[1]);
    } else {
        preg_match('%\s([^m\s]+mm)%i', $row['name'], $match);
        $foci = empty($match[1]) ? '' : $match[1];
        if(empty($foci)) {
            preg_match('%(\d{2,}-\d{2,})%i', $row['name'], $match);
            $foci = $match[1];
        }
        if(empty($foci)) {
            preg_match('%(\d+mm)%i', $row['name'], $match);
            $foci = $match[1];
        }       
           
        preg_match('%(f[/\d\.]+[^\s]+)%i', $row['name'], $match);
        $aperture = empty($match[1]) ? '' : $match[1];
        $search_string = "$foci $aperture";
    }


    $sphinx->SetMatchMode($mode[3]);
    $r1 = $sphinx->query($search_string, 'mysql');
    $ids1 = get_gid($r1);
    $sql = 'SELECT id FROM test.source_product_for_mapping WHERE product_name_full = ' . $m->quote($row['name']);
    $id = $m->Es($sql);
    if(!in_array($id, $ids1)) {
        array_push($ids1, $id);
    }

    $sphinx->SetMatchMode($mode[2]);
    $r2 = $sphinx->query($row['name'], 'mysql');
    $ids2 = get_gid($r2);

 
    $ids = array_intersect($ids1, $ids2); 
    
    if(!empty($ids)) {
        $sql = 'select domain_id, source_id, source_url from test.source_product_for_mapping where id in (' . implode(',', $ids) . ')';
        $source_info_list = $m->Eq($sql);
    } else {
        $source_info_list = array();
    }

    $count_list = count($source_info_list);
    for($j=0; $j<$count_list; $j++) {
        $source_info = $source_info_list[$j];
        if(4 == $source_info['domain_id']) {
            continue;
        }
        
        $now = date('Y-m-d H:i:s');
        $sql = "INSERT INTO test.object_mapping VALUES ('', '{$gids[$i]}', 1, '{$source_info['domain_id']}', '{$source_info['source_id']}', '{$source_info['source_url']}', '{$now}', '0', '1')";
        $m->Enq($sql);
    }

    $sphinx->resetFilters();
}

function get_gid($r) {
    return empty($r['matches']) ? array() : array_keys($r['matches']);
}
