<?php
set_time_limit(0);

include_once 'db.php';

//数据库连接
$m = new db();
$sql = 'select id from test.goods';
$gids = $m->Ec($sql);

//sphinx
$mode = array(SPH_MATCH_BOOLEAN, SPH_MATCH_EXTENDED2, SPH_MATCH_ANY, SPH_MATCH_ALL);
$sphinx = new SphinxClient();
$sphinx->SetServer('localhost', 9312);


/*
$sphinx->SetMatchMode($mode[2]);
$r2 = $sphinx->query('尼康AF-S 300mm f/2.8D IF-ED II', 'mysql');

var_dump($r2);
exit;
*/


//sp4m所有id
$sql = 'select id from test.source_product_for_mapping';
$sp4m_ids = $m->Ec($sql);

//goods匹配数大于一
$good_match_num = 0;


$count = count($gids);
for($i=0; $i<$count; $i++) {
    $sql = 'SELECT id,name,goods_model,url_aka,price,price_min,market_price,brand_id FROM test.goods WHERE id = ' . $gids[$i];
    $row = $m->Eor($sql);

    $sphinx->SetFilter('product_brand_id', array($row['brand_id']));
    preg_match('%([\d\.]+/[\d\.]+)%i', $row['name'], $match);
    if(!empty($match)) {
        $search_string = str_replace('/', ' ', $match[1]);
    } else {
        preg_match('%\s([^m\s]+mm)%i', $row['name'], $match);
        $foci = empty($match[1]) ? '' : $match[1];
        if(empty($foci)) {
            preg_match('%(\d{2,}-\d{2,})%i', $row['name'], $match);
            $foci = $match[1];
        }       
           
        preg_match('%(f[/\d\.]+[^\s]+)%i', $row['name'], $match);
        $aperture = empty($match[1]) ? '' : $match[1];
        $search_string = "$foci $aperture";
    }


    $sphinx->SetMatchMode($mode[3]);
    $r1 = $sphinx->query($search_string, 'mysql');
    $ids1 = get_gid($r1);
    $sql = 'SELECT id FROM test.source_product_for_mapping WHERE product_name_full = ' . $m->quote($row['name']);
    $id = $m->Es($sql);
    if(!in_array($id, $ids1)) {
        array_push($ids1, $id);
    }

    $sphinx->SetMatchMode($mode[2]);
    $r2 = $sphinx->query($row['name'], 'mysql');
    $ids2 = get_gid($r2);

  
    $ids = array_intersect($ids1, $ids2); 
    if(count($ids) > 1) {
        $good_match_num++;
    }

    $sp4m_ids = array_diff($sp4m_ids, $ids);
    
    $sphinx->resetFilters();
}


echo '<div>good match:&nbsp;&nbsp;' . $good_match_num . '</div>';
echo '<div>unmatch count:&nbsp;&nbsp;' . count($sp4m_ids) . '</div>';
echo '<div>they are:&nbsp;&nbsp;</div>';
var_dump($sp4m_ids);
echo 'done';

function get_gid($r) {
    return empty($r['matches']) ? array() : array_keys($r['matches']);
}
